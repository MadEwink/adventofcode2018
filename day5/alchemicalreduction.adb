with Ada.Text_IO; use Ada.Text_IO;
with Ada.Characters.Handling; use Ada.Characters.Handling;
with Ada.Containers; use Ada.Containers;

package body AlchemicalReduction is
	procedure ReadUnits(FileName : IN String ; Units : OUT UnitVectors.Vector) is
		File : File_Type;
		Current_Unit : Units_Type;
	begin
		Open(File, IN_FILE, FileName);
		while not End_Of_File(File) loop
			Get(File, Current_Unit);
			UnitVectors.Append(Units, Current_Unit);
		end loop;
		Close(File);
	end ReadUnits;
	procedure PrintUnits(Units : IN UnitVectors.Vector) is
	begin
		for Unit of Units loop
			Put(Unit);
		end loop;
		New_Line;
	end PrintUnits;
	procedure SimplifyPolymer(Units : IN OUT UnitVectors.Vector ; HasChanged : OUT Boolean) is
		Index : Natural;
	begin
		HasChanged := False;
		Index := UnitVectors.First_Index(Units);
		loop
			declare
				Current : Units_Type := Units(Index);
				Next : Units_Type := Units(Index+1);
			begin
				if (Current /= Next) and (To_Lower(Current) = To_Lower(Next)) then
					-- delete current
					UnitVectors.Delete(Units, Index);
					--delete next
					UnitVectors.Delete(Units, Index);
					HasChanged := True;
				else
					Index := Index+1;
				end if;
			end;
			exit when Index >= UnitVectors.Last_Index(Units)-1;
		end loop;
	end SimplifyPolymer;
	procedure TotalSimplification(Units : IN OUT UnitVectors.Vector ; FinalLength : OUT Count_Type) is
		HasChanged : Boolean;
	begin
		loop
			SimplifyPolymer(Units, HasChanged);
			exit when not HasChanged;
		end loop;
		FinalLength := UnitVectors.Length(Units);
	end TotalSimplification;
	procedure RemoveProblem(Units : IN OUT UnitVectors.Vector ; Problem : IN Units_Type) is
		Index : Natural;
	begin
		Index := UnitVectors.First_Index(Units);
		loop
			if (Units(Index) = Problem) or (To_Lower(Units(Index)) = To_Lower(Problem)) then
				UnitVectors.Delete(Units, Index);
			else
				Index := Index + 1;
			end if;
			exit when Index >= UnitVectors.Last_Index(Units);
		end loop;
	end RemoveProblem;
	procedure SearchProblem(Units : IN UnitVectors.Vector ; BestLength : OUT Count_Type) is
		UnitsNoProblem : UnitVectors.Vector;
	begin
		BestLength := UnitVectors.Length(Units);
		for Problem in Units_Type range 'a'..'z' loop
			declare
				Current_Lenght : Count_Type;
			begin
				UnitsNoProblem := Units;
				RemoveProblem(UnitsNoProblem, Problem);
				TotalSimplification(UnitsNoProblem, Current_Lenght);
				if Current_Lenght < BestLength then
					BestLength := Current_Lenght;
				end if;
			end;
		end loop;
	end SearchProblem;
end AlchemicalReduction;
