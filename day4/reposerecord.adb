with Ada.Text_IO; use Ada.Text_IO;

package body ReposeRecord is
	procedure ReadNumber(Line : IN String ; Last : IN Natural ; Number : OUT Natural ; IndexInc : OUT Natural) is
	begin
		Number := 0;
		IndexInc := 0;
		for c of Line(Line'First..Last) loop
			Number := Number * 10;
			IndexInc := IndexInc + 1;
			case c is
				when '1' => Number := Number +1;
				when '2' => Number := Number +2;
				when '3' => Number := Number +3;
				when '4' => Number := Number +4;
				when '5' => Number := Number +5;
				when '6' => Number := Number +6;
				when '7' => Number := Number +7;
				when '8' => Number := Number +8;
				when '9' => Number := Number +9;
				when '0' => Number := Number +0;
				when others =>
					Number := Number / 10;
					exit;
			end case;
		end loop;
	end ReadNumber;
	procedure Read_TimeStamp(Line : IN String ; Last : IN Natural ; TimeStamp : OUT TimeStamp_Type ; IndexInc : OUT Natural) is
		Index : Natural := Line'First+1;
	begin
		ReadNumber(Line(Index..Last), Last, TimeStamp.Date.Year, IndexInc);
		Index := Index + IndexInc;
		ReadNumber(Line(Index..Last), Last, TimeStamp.Date.Month, IndexInc);
		Index := Index + IndexInc;
		ReadNumber(Line(Index..Last), Last, TimeStamp.Date.Day, IndexInc);
		Index := Index + IndexInc;
		ReadNumber(Line(Index..Last), Last, TimeStamp.HourStamp.Hour, IndexInc);
		Index := Index + IndexInc;
		ReadNumber(Line(Index..Last), Last, TimeStamp.HourStamp.Minute, IndexInc);
		-- The total incrementation is last Index + last IndexInc
		IndexInc := Index+IndexInc;
	end Read_TimeStamp;
	function "="(Left, Right : IN TimeStamp_Type) return Boolean is
	begin
		return (Left.Date.Year = Right.Date.Year) and (Left.Date.Month = Right.Date.Month) and (Left.Date.Day = Right.Date.Day) and (Left.HourStamp.Hour = Right.HourStamp.Hour) and (Left.HourStamp.Minute = Right.HourStamp.Minute);
	end "=";
	function "<"(Left, Right : IN TimeStamp_Type) return Boolean is
	begin
		if Left.Date.Year /= Right.Date.Year then
			return Left.Date.Year < Right.Date.Year;
		elsif Left.Date.Month /= Right.Date.Month then
			return Left.Date.Month < Right.Date.Month;
		elsif Left.Date.Day /= Right.Date.Day then
			return Left.Date.Day < Right.Date.Day;
		elsif Left.HourStamp.Hour /= Right.HourStamp.Hour then
			return Left.HourStamp.Hour < Right.HourStamp.Hour;
		else
			return Left.HourStamp.Minute < Right.HourStamp.Minute;
		end if;
	end "<";
	procedure Print_TimeStamp(TimeStamp : IN TimeStamp_Type) is
	begin
		Put("(" & TimeStamp.Date.Year'Image & TimeStamp.Date.Month'Image & TimeStamp.Date.Day'Image & " -" & TimeStamp.HourStamp.Hour'Image & TimeStamp.HourStamp.Minute'Image & " )");
	end Print_TimeStamp;
	function "<"(Left, Right : IN Record_Type) return Boolean is
	begin
		return Left.TimeStamp < Right.TimeStamp;
	end "<";
	procedure ReadObservation(Line : IN String ; Last : IN Natural ; Observation : OUT Observation_Type) is
		Index : Natural := Line'First;
		IndexInc : Natural;
	begin
		case Line(Index) is
			when 'G' =>
			-- indicates guard shift
				Observation.Operation := SHIFT;
				Index := Index + 7;
				ReadNumber(Line(Index..Last), Last, Observation.Id, IndexInc);
			when 'f' =>
			-- indicates sleep
				Observation.Operation := SLEEP;
			when 'w' =>
			-- inducates wake
				Observation.Operation := WAKE;
			when others =>
				Put_Line(Standard_Error, "Wrong Operation");
		end case;
	end ReadObservation;
	procedure Print_Observation(Observation : IN Observation_Type) is
	begin
		case Observation.Operation is
			when SHIFT => Put("SHIFT");
			when SLEEP => Put("SLEEP");
			when WAKE => Put("WAKE");
		end case;
		Put(Observation.Id'Image);
	end Print_Observation;
	procedure ReadRecords(FileName : IN String ; Records : OUT RecordsVectors.Vector) is
		Input_File : File_Type;
		Line : String(1..50);
		Last : Natural;
		Current_Record : Record_Type;
		Index : Natural;
		IndexInc : Natural;
	begin
		Open(Input_File, IN_FILE, FileName);
		while not End_Of_File(Input_File) loop
			Get_Line(Input_File, Line, Last);
			Index := Line'First;
			Read_TimeStamp(Line, Last, Current_Record.TimeStamp, IndexInc);
			Index := Index + IndexInc;
			ReadObservation(Line(Index..Last), Last, Current_Record.Observation);
			RecordsVectors.Append(Records, Current_Record);
		end loop;
		Close(Input_File);
	end ReadRecords;
	procedure Print_Records(Records : IN RecordsVectors.Vector) is
	begin
		for Current_Record of Records loop
			Print_TimeStamp(Current_Record.TimeStamp);
			Put(" ");
			Print_Observation(Current_Record.Observation);
			New_Line;
		end loop;
	end Print_Records;
	procedure OrganizeRecords(Records : IN OUT RecordsVectors.Vector) is
		Current_Id : Natural := 0;
	begin
		-- Sort Records
		SortRecords.Sort(Records);
		-- Put correct Guard Id
		for Current_Record of Records loop
			case Current_Record.Observation.Operation is
				when SHIFT => Current_Id := Current_Record.Observation.Id;
				when others => Current_Record.Observation.Id := Current_Id;
			end case;
		end loop;
	end OrganizeRecords;
	function ComputeTimeInMinutes(BeginTime, EndTime : TimeStamp_Type) return Natural is
	begin
		return EndTime.HourStamp.Minute - BeginTime.HourStamp.Minute;
	end;
	procedure FindMostAsleepGuard(Records : IN RecordsVectors.Vector ; Id : OUT Natural) is
		type GuardSleep_Type is
			record
				Id : Natural;
				SleepTime : Natural;
			end record;
		package SleepCounters is new Ada.Containers.Vectors(Natural, GuardSleep_Type);
		SleepCounter : SleepCounters.Vector;
		Current_Id : Natural := 0;
	begin
		-- Compute total sleep time
		for Index in RecordsVectors.First_Index(Records)..RecordsVectors.Last_Index(Records) loop
			case Records(Index).Observation.Operation is
				when SHIFT =>
					Current_Id := Records(Index).Observation.Id;
					if (SleepCounters.Is_Empty(SleepCounter)) then
						declare
							GuardSleep : GuardSleep_Type;
						begin
							GuardSleep.Id := Current_Id;
							GuardSleep.SleepTime := 0;
							SleepCounters.Append(SleepCounter, GuardSleep);
						end;
					else
						declare
							GuardFound : Boolean := False;
							GuardSleep : GuardSleep_Type;
						begin
							for Guard of SleepCounter loop
								if Guard.Id = Current_Id then
									GuardFound := True;
									exit;
								end if;
							end loop;
							if not GuardFound then
								GuardSleep.Id := Current_Id;
								GuardSleep.SleepTime := 0;
								SleepCounters.Append(SleepCounter, GuardSleep);
							end if;
						end;
					end if;
				when SLEEP =>
					declare
						SleepTime : Natural;
					begin
						-- compute sleep time
						SleepTime := ComputeTimeInMinutes(Records(Index).TimeStamp, Records(Index+1).TimeStamp);
						-- find guard and add time
						for Guard of SleepCounter loop
							if Guard.Id = Current_Id then
								Guard.SleepTime := Guard.SleepTime+SleepTime;
							end if;
						end loop;
					end;
				when others =>
					null;
			end case;
		end loop;
		-- Find Most Asleep
		declare
			MostAsleepGuard : GuardSleep_Type := (0,0);
		begin
			for Guard of SleepCounter loop
				if Guard.SleepTime > MostAsleepGuard.SleepTime then
					MostAsleepGuard.Id := Guard.Id;
					MostAsleepGuard.SleepTime := Guard.SleepTime;
					Id := MostAsleepGuard.Id;
				end if;
			end loop;
		end;
	end FindMostAsleepGuard;
	procedure FindMostAlseepMinute(Records : IN RecordsVectors.Vector ; Id : IN Natural ; Minute, SleepOccurencies : OUT Natural) is
		type Minute_Array is array (0..59) of Natural;
		MinutesAsleep : Minute_Array := (others => 0);
		Current_Id : Natural := 0;
		MostMinute, TimesSleep : Natural := 0;
	begin
		-- compute each minute sleep
		for Index in RecordsVectors.First_Index(Records)..RecordsVectors.Last_Index(Records) loop
			case Records(Index).Observation.Operation is
				when SHIFT =>
					Current_Id := Records(Index).Observation.Id;
				when SLEEP =>
					if Current_Id = Id then
						for Current_Minute in Records(Index).TimeStamp.HourStamp.Minute..Records(Index+1).TimeStamp.HourStamp.Minute-1 loop
							MinutesAsleep(Current_Minute) := MinutesAsleep(Current_Minute) + 1;
						end loop;
					end if;
				when others =>
					null;
			end case;
		end loop;
		-- Find minute
		for Current_Minute in 0..59 loop
			if MinutesAsleep(Current_Minute) > TimesSleep then
				MostMinute := Current_Minute;
				TimesSleep := MinutesAsleep(Current_Minute);
			end if;
		end loop;
		Minute := MostMinute;
		SleepOccurencies := TimesSleep;
	end FindMostAlseepMinute;
	procedure Strategy2(Records : IN RecordsVectors.Vector; Result : OUT Natural) is
		type GuardSleep_Type is
			record
				Id : Natural;
				Minute : Natural;
				SleepOccurencies : Natural;
			end record;
		package SleepCounters is new Ada.Containers.Vectors(Natural, GuardSleep_Type);
		SleepCounter : SleepCounters.Vector;
		Current_Id : Natural;
		MostAsleepGuard : GuardSleep_Type := (0, 0, 0);
	begin
		-- list all guards
		for Current_Record of Records loop
			if (Current_Record.Observation.Operation = SHIFT) then
				Current_Id := Current_Record.Observation.Id;
				if (SleepCounters.Is_Empty(SleepCounter)) then
					declare
						GuardSleep : GuardSleep_Type;
					begin
						GuardSleep.Id := Current_Id;
						SleepCounters.Append(SleepCounter, GuardSleep);
					end;
				else
					declare
						GuardFound : Boolean := False;
						GuardSleep : GuardSleep_Type;
					begin
						for Guard of SleepCounter loop
							if Guard.Id = Current_Id then
								GuardFound := True;
								exit;
							end if;
						end loop;
						if not GuardFound then
							GuardSleep.Id := Current_Id;
							SleepCounters.Append(SleepCounter, GuardSleep);
						end if;
					end;
				end if;
			end if;
		end loop;
		-- compute most sleep minute and find the one with biggest frenquency
		for Guard of SleepCounter loop
			FindMostAlseepMinute(Records, Guard.Id, Guard.Minute, Guard.SleepOccurencies);
			if (Guard.SleepOccurencies > MostAsleepGuard.SleepOccurencies) then
				MostAsleepGuard.Id := Guard.Id;
				MostAsleepGuard.Minute := Guard.Minute;
				MostAsleepGuard.SleepOccurencies := Guard.SleepOccurencies;
			end if;
		end loop;
		Result := MostAsleepGuard.Id*MostAsleepGuard.Minute;
	end Strategy2;
end ReposeRecord;
