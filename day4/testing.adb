with ReposeRecord; use ReposeRecord;
with Ada.Text_IO; use Ada.Text_IO;

procedure Testing is
	procedure Test_Read_TimeStamp is
		Lines : array (1..17) of String(1..18) := (
			1 => "[1518-11-01 00:00]",
			2 => "[1518-11-01 00:05]",
			3 => "[1518-11-01 00:25]",
			4 => "[1518-11-01 00:30]",
			5 => "[1518-11-01 00:55]",
			6 => "[1518-11-01 23:58]",
			7 => "[1518-11-02 00:40]",
			8 => "[1518-11-02 00:50]",
			9 => "[1518-11-03 00:05]",
			10 => "[1518-11-03 00:24]",
			11 => "[1518-11-03 00:29]",
			12 => "[1518-11-04 00:02]",
			13 => "[1518-11-04 00:36]",
			14 => "[1518-11-04 00:46]",
			15 => "[1518-11-05 00:03]",
			16 => "[1518-11-05 00:45]",
			17 => "[1518-11-05 00:55]");
		Expected_TimeStamps : array(1..17) of TimeStamp_Type := (
			1 => (Date => (1518,11,1), HourStamp => (0,0)),
			2 => (Date => (1518,11,1), HourStamp => (0,5)),
			3 => (Date => (1518,11,01), HourStamp => (00,25)),
			4 => (Date => (1518,11,01), HourStamp => (00,30)),
			5 => (Date => (1518,11,01), HourStamp => (00,55)),
			6 => (Date => (1518,11,01), HourStamp => (23,58)),
			7 => (Date => (1518,11,02), HourStamp => (00,40)),
			8 => (Date => (1518,11,02), HourStamp => (00,50)),
			9 => (Date => (1518,11,03), HourStamp => (00,05)),
			10 => (Date => (1518,11,03), HourStamp => (00,24)),
			11 => (Date => (1518,11,03), HourStamp => (00,29)),
			12 => (Date => (1518,11,04), HourStamp => (00,02)),
			13 => (Date => (1518,11,04), HourStamp => (00,36)),
			14 => (Date => (1518,11,04), HourStamp => (00,46)),
			15 => (Date => (1518,11,05), HourStamp => (00,03)),
			16 => (Date => (1518,11,05), HourStamp => (00,45)),
			17 => (Date => (1518,11,05), HourStamp => (00,55)));
		TimeStamp : TimeStamp_Type;
		IndexInc : Natural;
	begin
		for Index in 1..12 loop
			Read_TimeStamp(Lines(Index), 18, TimeStamp, IndexInc);
			if not (Expected_TimeStamps(Index) = TimeStamp) then
				Put("TimeStamp " & Lines(Index) & " gave wrong result ");
				Print_TimeStamp(TimeStamp);
				New_Line;
			end if;
		end loop;
	end Test_Read_TimeStamp;
	procedure Test_ReadRecords is
		Records : RecordsVectors.Vector;
	begin
		ReadRecords("input", Records);
		Print_Records(Records);
	end Test_ReadRecords;
	procedure Test_OrganizeRecords is
		Records : RecordsVectors.Vector;
	begin
		ReadRecords("input", Records);
		Print_Records(Records);
		OrganizeRecords(Records);
		Print_Records(Records);
	end Test_OrganizeRecords;
	procedure Test_FindMostAsleepGuard is
		Records : RecordsVectors.Vector;
		Id : Natural;
	begin
		ReadRecords("input", Records);
		OrganizeRecords(Records);
		FindMostAsleepGuard(Records, Id);
		Put_Line(Id'Image);
	end Test_FindMostAsleepGuard;
	procedure Test_FindMostAlseepMinute is
		Records : RecordsVectors.Vector;
		Id : Natural;
		Minute : Natural;
		Occ : Natural;
		Result : Natural;
	begin
		ReadRecords("input", Records);
		OrganizeRecords(Records);
		--Print_Records(Records);
		FindMostAsleepGuard(Records, Id);
		FindMostAlseepMinute(Records, Id, Minute, Occ);
		Result := Id*Minute;
		Put_Line(Result'Image);
	end Test_FindMostAlseepMinute;
	procedure Test_Strategy2 is
		Records : RecordsVectors.Vector;
		Result : Natural;
	begin
		ReadRecords("input", Records);
		OrganizeRecords(Records);
		Strategy2(Records, Result);
		Put_Line(Result'Image);
	end Test_Strategy2;
begin
	Test_FindMostAlseepMinute;
	Test_Strategy2;
end Testing;
