with Ada.Containers.Vectors;

package ReposeRecord is
	type Date_Type is
		record
			Year : Natural;
			Month : Natural;
			Day : Natural;
		end record;
	type HourStamp_Type is
		record
			Hour : Natural;
			Minute : Natural;
		end record;
	type TimeStamp_Type is
		record
			Date : Date_Type;
			HourStamp : HourStamp_Type;
		end record;
	procedure Read_TimeStamp(Line : IN String ; Last : IN Natural ; TimeStamp : OUT TimeStamp_Type ; IndexInc : OUT Natural);
	function "="(Left, Right : IN TimeStamp_Type) return Boolean;
	function "<"(Left, Right : IN TimeStamp_Type) return Boolean;
	procedure Print_TimeStamp(TimeStamp : IN TimeStamp_Type);
	type Operation_Type is (SHIFT, SLEEP, WAKE);
	type Observation_Type is
		record
			Operation : Operation_Type;
			Id : Natural;
		end record;
	type Record_Type is
		record
			TimeStamp : TimeStamp_Type;
			Observation : Observation_Type;
		end record;
	function "<"(Left, Right : IN Record_Type) return Boolean;
	package RecordsVectors is new Ada.Containers.Vectors(Natural, Record_Type);
	package SortRecords is new RecordsVectors.Generic_Sorting;
	procedure ReadObservation(Line : IN String ; Last : IN Natural ; Observation : OUT Observation_Type);
	procedure Print_Observation(Observation : IN Observation_Type);
	procedure ReadRecords(FileName : IN String ; Records : OUT RecordsVectors.Vector);
	procedure Print_Records(Records : IN RecordsVectors.Vector);
	procedure OrganizeRecords(Records : IN OUT RecordsVectors.Vector);
	procedure FindMostAsleepGuard(Records : IN RecordsVectors.Vector ; Id : OUT Natural);
	procedure FindMostAlseepMinute(Records : IN RecordsVectors.Vector ; Id : IN Natural ; Minute, SleepOccurencies : OUT Natural);
	procedure Strategy2(Records : IN RecordsVectors.Vector; Result : OUT Natural);
end ReposeRecord;
