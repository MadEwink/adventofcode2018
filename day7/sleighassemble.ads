with Ada.Containers.Vectors;

package SleighAssemble is
	type Step_Type;
	type Step_Acc is access all Step_Type;
	package RequisiteVectors is new Ada.Containers.Vectors(Natural, Character);
	type Step_Type is
		record
			Id : Character;
			Is_Available : Boolean;
			Is_Ready : Boolean;
			ReadyTime : Natural;
			RequisiteIds : RequisiteVectors.Vector;
		end record;
	package StepVectors is new Ada.Containers.Vectors(Natural, Step_Type);
	procedure ReadSteps (FileName : IN String ; Steps : OUT StepVectors.Vector);
	procedure PrintSteps(Steps : StepVectors.Vector);
	procedure DoAssemble(FileName : IN String);
	procedure DoAssemble(FileName : IN String ; WorkerNumber : IN Positive ; StepBaseTime : IN Natural);
end SleighAssemble;
