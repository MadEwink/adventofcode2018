with Ada.Text_IO; use Ada.Text_IO;
with Ada.Containers.Generic_Array_Sort;

package body SleighAssemble is
	use StepVectors;
	use RequisiteVectors;
	procedure String_Sort is new Ada.Containers.Generic_Array_Sort(Positive, Character, String);
	function GetStepAcc(Steps : IN OUT StepVectors.Vector ; Id : IN Character) return Step_Acc is
		StepAcc : Step_Acc;
	begin
		for CurrentStep of Steps loop
			if CurrentStep.Id = Id then
				StepAcc := CurrentStep'access;
				return StepAcc;
			end if;
		end loop;
		declare
			NewStep : Step_Type;
		begin
			NewStep.Id := Id;
			NewStep.Is_Available := True;
			NewStep.Is_Ready := False;
			NewStep.ReadyTime := 0;
			Append(Steps, NewStep);
			return GetStepAcc(Steps, Id);
		end;
	end GetStepAcc;
	procedure ProcessInput(Steps : IN OUT StepVectors.Vector ; RequisiteId, StepId : IN Character) is
		Req, Step : Step_Acc;
	begin
		Req := GetStepAcc(Steps, RequisiteId);
		Step := GetStepAcc(Steps, StepId);
		Step.all.Is_Available := False;
		Append(Step.all.RequisiteIds, RequisiteId);
	end ProcessInput;
	procedure ReadSteps (FileName : IN String ; Steps : OUT StepVectors.Vector) is
		Input_File : File_Type;
		Line : String(1..49);
		Last : Natural;
	begin
		Open(Input_File, IN_FILE, FileName);
		while not End_Of_File(Input_File) loop
			Get_Line(Input_File, Line, Last);
			ProcessInput(Steps, Line(6), Line(37));
		end loop;
		Close(Input_File);
	end ReadSteps;
	procedure PrintSteps(Steps : StepVectors.Vector) is
	begin
		for Step of Steps loop
			Put(Step.Id & " (" & "available : " & Step.Is_Available'Image & ", ready : " & Step.Is_Ready'Image & ") :");
			for Req of Step.RequisiteIds loop
				Put(" " & Req & ",");
			end loop;
			New_Line;
		end loop;
	end PrintSteps;
	function DetermineNext(Steps : IN StepVectors.Vector) return Character is
		AvailableNumber : Natural := 0;
	begin
		for Step of Steps loop
			if Step.Is_Available and not Step.Is_Ready then
				AvailableNumber := AvailableNumber + 1;
			end if;
		end loop;
		declare
			AvailableSteps : String(1..AvailableNumber);
			CurrentIndex : Positive := 1;
		begin
			for Step of Steps loop
				if Step.Is_Available and not Step.Is_Ready then
					AvailableSteps(CurrentIndex) := Step.Id;
					CurrentIndex := CurrentIndex + 1;
				end if;
			end loop;
			String_Sort(AvailableSteps);
			return AvailableSteps(1);
		end;
	end DetermineNext;
	procedure UpdateAvailability(Steps : IN OUT StepVectors.Vector) is
		Is_Ready : Boolean;
		ReqAcc : Step_Acc;
	begin
		for Step of Steps loop
			if not Step.Is_Available then
				Is_Ready := True;
				for Req of Step.RequisiteIds loop
					ReqAcc := GetStepAcc(Steps, Req);
					if not ReqAcc.all.Is_Ready then
						Is_Ready := False;
						exit;
					end if;
				end loop;
				Step.Is_Available := Is_Ready;
			end if;
		end loop;
	end UpdateAvailability;
	procedure DoAssemble(FileName : IN String) is
		Steps : StepVectors.Vector;
		Is_Finished : Boolean;
		NextStepId : Character;
		StepAcc : Step_Acc;
	begin
		ReadSteps(FileName, Steps);
		loop
			Is_Finished := True;
			for Step of Steps loop
				if not Step.Is_Ready then
					Is_Finished := False;
					exit;
				end if;
			end loop;
			exit when Is_Finished;
			NextStepId := DetermineNext(Steps);
			Put(NextStepId);
			StepAcc := GetStepAcc(Steps, NextStepId);
			StepAcc.all.Is_Ready := True;
			UpdateAvailability(Steps);
		end loop;
		New_Line;
	end DoAssemble;
	function GetSupTime(StepId : IN Character) return Positive is
	begin
		return 1 + Character'Pos(StepId) - Character'Pos('A');
	end GetSupTime;
	function DetermineNextAfter(Steps : IN StepVectors.Vector ; StepId : IN Character) return Character is
		AvailableNumber : Natural := 0;
	begin
		for Step of Steps loop
			if Step.Is_Available and not Step.Is_Ready and Step.Id > StepId then
				AvailableNumber := AvailableNumber + 1;
			end if;
		end loop;
		if AvailableNumber = 0 then
			return ' ';
		end if;
		declare
			AvailableSteps : String(1..AvailableNumber);
			CurrentIndex : Positive := 1;
		begin
			for Step of Steps loop
				if Step.Is_Available and not Step.Is_Ready and Step.Id > StepId then
					AvailableSteps(CurrentIndex) := Step.Id;
					CurrentIndex := CurrentIndex + 1;
				end if;
			end loop;
			String_Sort(AvailableSteps);
			return AvailableSteps(1);
		end;
	end DetermineNextAfter;
	procedure DoAssemble(FileName : IN String ; WorkerNumber : IN Positive ; StepBaseTime : IN Natural) is
		Steps : StepVectors.Vector;
		CurrentSteps : String(1..WorkerNumber) := (others => ' ');
		CurrentTime : Natural := 0;
		Is_Finished : Boolean;
		StepAcc : Step_Acc;
	begin
		ReadSteps(FileName, Steps);
		loop
			Is_Finished := True;
			for Step of Steps loop
				if not Step.Is_Ready then
					Is_Finished := False;
					exit;
				end if;
			end loop;
			exit when Is_Finished;
			-- assign tasks
			declare
				Index : Positive := 1;
				StepId : Character;
				procedure NoDoubleAssign is
					HasChanged : Boolean;
				begin
					loop
						HasChanged := False;
						for AssignedStep of CurrentSteps loop
							exit when StepId = ' ';
							if AssignedStep = StepId then
								StepId := DetermineNextAfter(Steps, StepId);
								HasChanged := True;
							end if;
						end loop;
						exit when not HasChanged;
					end loop;
				end NoDoubleAssign;
			begin
				StepId := DetermineNext(Steps);
				NoDoubleAssign;
				loop
					exit when StepId = ' '; -- no available task
					if CurrentSteps(Index) = ' ' then
						CurrentSteps(Index) := StepId;
						StepAcc := GetStepAcc(Steps, StepId);
						StepAcc.all.ReadyTime := StepBaseTime + GetSupTime(StepId) + CurrentTime - 1;
						StepId := DetermineNextAfter(Steps, StepId);
						NoDoubleAssign;
					end if;
					Index := Index + 1;
					exit when Index > WorkerNumber;
				end loop;
			end;
			-- update readyness
			for AssignedStep of CurrentSteps loop
				if AssignedStep /= ' ' then
					StepAcc := GetStepAcc(Steps, AssignedStep);
					if StepAcc.all.ReadyTime = CurrentTime then
						StepAcc.all.Is_Ready := True;
						AssignedStep := ' ';
					end if;
				end if;
			end loop;
			CurrentTime := CurrentTime + 1;
			UpdateAvailability(Steps);
		end loop;
		Put_Line(CurrentTime'Image);
	end DoAssemble;
end SleighAssemble;
