with Ada.Text_IO; use Ada.Text_IO;

package body MemoryManeuver is
	use Node_Trees;
	procedure ReadNode(File : IN OUT File_Type ; Nodes : IN OUT Node_Trees.Tree ; C : IN Cursor) is
		NewNode : Node_Type;
		package Int_IO is new Integer_IO(Natural);
		Current_Cursor : Cursor;
	begin
		Int_IO.Get(File, NewNode.ChildNumber);
		Int_IO.Get(File, NewNode.MetadataNumber);
		Insert_Child(Nodes,
			C,
			No_Element,
			NewNode,
			Current_Cursor);
		for Index in 1..NewNode.ChildNumber loop
			ReadNode(File, Nodes, Current_Cursor);
		end loop;
		for Index in 1..NewNode.MetadataNumber loop
			declare
				Metadata : Natural;
			begin
				Int_IO.Get(File, Metadata);
				MetadataVectors.Append(NewNode.Metadatas, Metadata);
			end;
		end loop;
		Replace_Element(Nodes,
			Current_Cursor,
			NewNode);
	end ReadNode;
	procedure ReadNodes(FileName : IN String ; Nodes : OUT Node_Trees.Tree) is
		Input_File : File_Type;
	begin
		Open(Input_File, IN_FILE, FileName);
		ReadNode(Input_File, Nodes, Root(Nodes));
		Close(Input_File);
	end ReadNodes;
	procedure PrintNode(Node : IN Node_Type) is
	begin
		Put(Node.ChildNumber'Image);
		Put(Node.MetadataNumber'Image);
		Put(" (");
		for Metadata of Node.Metadatas loop
			Put(Natural'Image(Metadata));
		end loop;
		Put(" ) :");
		Put(Node.Value'Image);
		New_Line;
	end PrintNode;
	procedure RecursivePrintNode(Nodes : IN Tree ; Current_Cursor : IN Cursor ; Depth : IN Natural) is
	begin
		for I in 1..Depth loop
			Put(' ');
		end loop;
		PrintNode(Nodes(Current_Cursor));
		for C in Iterate_Children(Nodes, Current_Cursor) loop
			RecursivePrintNode(Nodes, C, Depth+1);
		end loop;
	end RecursivePrintNode;
	procedure PrintNodes(Nodes : IN Node_Trees.Tree) is
	begin
		for C in Iterate_Children(Nodes, Root(Nodes)) loop
			RecursivePrintNode(Nodes, C, 0);
		end loop;
	end PrintNodes;
	function SumOfNodeMetadata(Node : IN Node_Type) return Natural is
		Sum : Natural := 0;
	begin
		for Metadata of Node.Metadatas loop
			Sum := Sum + Metadata;
		end loop;
		return Sum;
	end SumOfNodeMetadata;
	procedure ComputeMetadataSum(FileName : IN String) is
		Nodes : Tree;
		Sum : Natural := 0;
	begin
		ReadNodes(FileName, Nodes);
		for C in Iterate(Nodes) loop
			Sum := Sum + SumOfNodeMetadata(Nodes(C));
		end loop;
		Put_Line(Sum'Image);
	end ComputeMetadataSum;
	function GetNthChildValue(Nodes : IN Tree ; Current_Cursor : IN Cursor ; N : IN Natural) return Natural is
		Index : Natural := 1;
	begin
		for C in Iterate_Children(Nodes, Current_Cursor) loop
			if (Index = N) then
				return Nodes(C).Value;
			end if;
			Index := Index + 1;
		end loop;
		return 0;
	end GetNthChildValue;
	procedure ComputeNodeValue(Nodes : IN OUT Tree ; Current_Cursor : IN Cursor) is
	begin
		if Nodes(Current_Cursor).ChildNumber = 0 then
			Nodes(Current_Cursor).Value := SumOfNodeMetadata(Nodes(Current_Cursor));
		else
			for C in Iterate_Children(Nodes, Current_Cursor) loop
				ComputeNodeValue(Nodes, C);
			end loop;
			declare
				Value : Natural := 0;
			begin
				for Index of Nodes(Current_Cursor).Metadatas loop
					Value := Value + GetNthChildValue(Nodes, Current_Cursor, Index);
				end loop;
				Nodes(Current_Cursor).Value := Value;
			end;
		end if;
	end ComputeNodeValue;
	procedure ComputeRootValue(FileName : IN String) is
		Nodes : Tree;
	begin
		ReadNodes(FileName, Nodes);
		ComputeNodeValue(Nodes, First_Child(Root(Nodes)));
		Put_Line(Natural'Image(Nodes(First_Child(Root(Nodes))).Value));
	end ComputeRootValue;
end MemoryManeuver;
