with Text_IO; use Text_IO;

package body StarsAlign is
	function "+"(Left : StarPosition_Type ; Right : StarVelocity_Type) return StarPosition_Type is
		Result : StarPosition_Type;
	begin
		Result.X := Left.X + Right.X;
		Result.Y := Left.Y + Right.Y;
		return Result;
	end "+";
	procedure PrintCoordinate(C : Coordinates_Type) is
	begin
		Put("(" & C.X'Image & "," & C.Y'Image & " )");
	end PrintCoordinate;
	procedure ReadStars(FileName : IN String ; Stars : OUT StarVectors.Vector) is
		Input_File : File_Type;
		Current_Star : Star_Type;
		package Int_IO is new Integer_IO(Integer);
		procedure ReadCoordinate(File : IN OUT File_Type ; Coordinate : IN OUT Coordinates_Type) is
			Item : Character;
		begin
			loop
				Get(File, Item);
				exit when Item = '<';
			end loop;
			Int_IO.Get(File, Coordinate.X);
			Get(File, Item);
			if Item /= ',' then
				Put_Line(Standard_Error, "Unexpected '" & Item & "' in the file, expected ','");
				raise Use_Error;
			end if;
			Int_IO.Get(File, Coordinate.Y);
		end ReadCoordinate;
	begin
		Open(Input_File, IN_FILE, FileName);
		loop
			ReadCoordinate(Input_File, Current_Star.Position);
			ReadCoordinate(Input_File, Current_Star.Velocity);
			StarVectors.Append(Stars, Current_Star);
			Skip_Line(Input_File);
			exit when End_Of_File(Input_File);
		end loop;
		Close(Input_File);
	end ReadStars;
	procedure PrintStars(Stars : IN StarVectors.Vector) is
	begin
		for Star of Stars loop
			PrintCoordinate(Star.Position);
			Put(" : ");
			PrintCoordinate(Star.Velocity);
			New_Line;
		end loop;
	end PrintStars;
	procedure UpdateStarsPosition(Stars : IN OUT StarVectors.Vector) is
	begin
		for Star of Stars loop
			Star.Position := Star.Position + Star.Velocity;
		end loop;
	end UpdateStarsPosition;
	procedure FindMinMax(Stars : IN StarVectors.Vector ; Min,Max : OUT StarPosition_Type) is
	begin
		Min := Stars(0).Position;
		Max := Stars(0).Position;
		for Star of Stars loop
			if Star.Position.X < Min.X then
				Min.X := Star.Position.X;
			end if;
			if Star.Position.Y < Min.Y then
				Min.Y := Star.Position.Y;
			end if;
			if Star.Position.X > Max.X then
				Max.X := Star.Position.X;
			end if;
			if Star.Position.Y > Max.Y then
				Max.Y := Star.Position.Y;
			end if;
		end loop;
	end FindMinMax;
	procedure PrintStarsInSky(File : IN File_Type ; Stars : IN StarVectors.Vector ; Min,Max : IN StarPosition_Type) is
		Display : array (0..Max.Y-Min.Y) of String(1..Max.X-Min.X+1) := ( others => ( others =>' '));
	begin
		for Star of Stars loop
			Display(Star.Position.Y-Min.Y)(Star.Position.X-Min.X+1) := '#';
		end loop;
		for Line of Display loop
			Put_Line(File, Line);
		end loop;
	end PrintStarsInSky;
	function FindMinimumDisparity(Stars : IN StarVectors.Vector) return Natural is
		StepAheads : Natural := 0;
		Previous_Min,Previous_Max : StarPosition_Type;
		Current_Min,Current_Max : StarPosition_Type;
		StarsCopy : StarVectors.Vector := Stars;
		function Y_Disparity(Min, Max : IN StarPosition_Type) return Natural is
		begin
			return Max.Y - Min.Y;
		end Y_Disparity;
	begin
		FindMinMax(StarsCopy, Previous_Min, Previous_Max);
		loop
			UpdateStarsPosition(StarsCopy);
			StepAheads := StepAheads + 1;
			FindMinMax(StarsCopy, Current_Min, Current_Max);
			exit when Y_Disparity(Current_Min,Current_Max) > Y_Disparity(Previous_Min, Previous_Max);
		end loop;
		return StepAheads - 1;
	end FindMinimumDisparity;
	procedure ProcessMessageInTheSky(FileName : IN String) is
		Stars : StarVectors.Vector;
		Input : Natural;
		Min, Max : StarPosition_Type;
		package Nat_IO is new Integer_IO(Natural);
	begin
		ReadStars(FileName, Stars);
		loop
			FindMinMax(Stars, Min, Max);
			Put("Min : ");
			PrintCoordinate(Min);
			Put(", Max : ");
			PrintCoordinate(Max);
			New_Line;
			Put_Line("0 : Quit ; 1 : Compute next ; 2 : Print sky std ; 3 : Print sky file ; 4 : Find minimum disparity ; 5 : 10 steps ; 6 : 100 steps");
			Put("Your choice : ");
			Nat_IO.Get(Input);
			case Input is
				when 0 => exit;
				when 1 => UpdateStarsPosition(Stars);
				when 2 => PrintStarsInSky(Standard_Output, Stars, Min, Max);
				when 3 =>
					declare
						OutputFile : File_Type;
					begin
						Create(OutputFile, OUT_FILE, "output");
						PrintStarsInSky(OutputFile, Stars, Min, Max);
						Close(OutputFile);
					end;
				when 4 =>
					declare
						StepAheads : Natural := FindMinimumDisparity(Stars);
					begin
						Put_Line("Next local minimum is in" & StepAheads'Image & " steps");
					end;
				when 5 =>
					for I in 1..10 loop
						UpdateStarsPosition(Stars);
					end loop;
				when 6 =>
					for I in 1..100 loop
						UpdateStarsPosition(Stars);
					end loop;
				when 7 =>
					for I in 1..1000 loop
						UpdateStarsPosition(Stars);
					end loop;
				when others => null;
			end case;
		end loop;
	end ProcessMessageInTheSky;
end StarsAlign;
