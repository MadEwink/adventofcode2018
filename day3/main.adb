with Ada.Text_IO; use Ada.Text_IO;
with Slicer; use Slicer;

procedure Main is
	Claims : ClaimVectors.Vector;
	MaxX, MaxY : Natural;
	Count : Natural;
begin
	ReadAllClaims(Claims);
	FindMaxXY(Claims, MaxX, MaxY);
	Put_Line(MaxX'Image & MaxY'Image);
	CountOverlap(Claims, MaxX, MaxY, Count);
	Put_Line(Count'Image);
end Main;
