with Ada.Containers.Vectors;

package Slicer is
	-- Rect Coordinates
	type Rect is
		record
			X : Natural;
			Y : Natural;
		end record;
	type Claim_Type is
		record
			Id : Natural;
			Offset : Rect;
			Size : Rect;
		end record;
	package ClaimVectors is new Ada.Containers.Vectors (Natural, Claim_Type);
	procedure ParseClaimString(ClaimString : IN String ; Last : IN Natural ; Claim : OUT Claim_Type);
	procedure ReadAllClaims(Claims : IN OUT ClaimVectors.Vector);
	procedure PrintClaim(Claim : IN Claim_Type);
	procedure PrintAllClaims(Claims : IN ClaimVectors.Vector);
	procedure FindMaxXY(Claims : IN ClaimVectors.Vector; MaxX, MaxY : OUT Natural);
	procedure CountOverlap(Claims : IN ClaimVectors.Vector; MaxX, MaxY : IN Natural; OverlapingCount : OUT Natural);
end Slicer;
