with Ada.Text_IO;
with Ada.Containers.Vectors;

package InventoryScan is
	type CharCount is array (Character) of Natural;
	subtype Word_Type is String (1..30);
	type BoxName_Type is
		record
			Word : Word_Type;
			Last : Natural;
		end record;
	-- Check if Word contains a character exactly two or three times
	procedure AnalyzeWord(Word : IN String ; ContainsTwo, ContainsThree : OUT Boolean);
	-- Open input file, read words and analyze them, returns checksum
	function DoScan return Natural;
	package BoxNames is new Ada.Containers.Vectors (Natural, BoxName_Type);
	procedure ReadWords(Names : IN OUT BoxNames.Vector);
	procedure FindBoxes;
end InventoryScan;
