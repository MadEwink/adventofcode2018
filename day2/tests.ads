with Ada.Text_IO;
with InventoryScan;

package Tests is
	function TestAnalyzeWord return Boolean;
	function DoAllTests return Boolean;
end Tests;
