with Ada.Text_IO;
with InventoryScan;

package body Tests is
	function TestAnalyzeWord return Boolean is
		function TestWord(Word : IN String ; ExpectedTwo, ExpectedThree : IN Boolean) return Boolean is
			OutputTwo, OutputThree : Boolean;
		begin
			InventoryScan.AnalyzeWord(Word, OutputTwo, OutputThree);
			return OutputTwo = ExpectedTwo and OutputThree = ExpectedThree;
		end TestWord;
	begin
		return TestWord("abcdef", False, False)
		and TestWord("bababc", True, True)
		and TestWord("abbcde", True, False)
		and TestWord("abcccd", False, True)
		and TestWord("aabcdd", True, False)
		and TestWord("abcdee", True, False)
		and TestWord("ababab", False, True);
	end TestAnalyzeWord;
	function DoAllTests return Boolean is
	begin
		return TestAnalyzeWord;
	end DoAllTests;
end Tests;
