with InventoryScan;
with Tests;
with Ada.Text_IO; use Ada.Text_IO;

procedure Main is
	Checksum : Natural;
begin
	if not Tests.DoAllTests then
		Put_Line("Tests failed");
		return;
	end if;
	Checksum := InventoryScan.DoScan;
	Put_Line("Checksum :" & Checksum'Image);
	InventoryScan.FindBoxes;
end Main;
