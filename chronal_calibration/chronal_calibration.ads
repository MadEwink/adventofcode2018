with ADA.TEXT_IO;

package CHRONAL_CALIBRATION is
	UNKNOWN_OPERATOR : EXCEPTION;
	EXISTING_FILE : EXCEPTION;
	procedure READ_INPUT(FILE : IN OUT ADA.TEXT_IO.FILE_TYPE; VALUE : OUT INTEGER);
	function ADD_FREQUENCY(FREQUENCY : IN INTEGER) return BOOLEAN;
	procedure WRITE_FREQUENCY(FILE : ADA.TEXT_IO.FILE_TYPE; FREQUENCY : IN INTEGER);
end CHRONAL_CALIBRATION;

