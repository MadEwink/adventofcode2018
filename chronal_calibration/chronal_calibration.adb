with ADA.TEXT_IO;
with ADA.IO_EXCEPTIONS;
use ADA.TEXT_IO;

package body CHRONAL_CALIBRATION is
	procedure READ_INPUT (FILE : IN OUT FILE_TYPE;
			VALUE : OUT INTEGER) is
		S : STRING(1..10);
		LEN : INTEGER;
	begin
		GET_LINE(FILE, S, LEN);
		case S(S'FIRST) is
			when '+' => VALUE := INTEGER'VALUE(S(S'FIRST+1..LEN));
			when '-' => VALUE := -INTEGER'VALUE(S(S'FIRST+1..LEN));
			when others => raise UNKNOWN_OPERATOR;
		end case;
	end READ_INPUT;
	function ADD_FREQUENCY (FREQUENCY : IN INTEGER) return BOOLEAN is
		FILE_FREQ : FILE_TYPE;
		VALUE : INTEGER;
		ADDED : BOOLEAN;
		S : STRING(1..10);
		LEN : INTEGER;
	begin
		OPEN(FILE_FREQ, IN_FILE, "visited_frequencies");
		ADDED := FALSE;
		while not(END_OF_FILE(FILE_FREQ)) loop
			GET_LINE(FILE_FREQ,S, LEN);
			VALUE := INTEGER'VALUE(S(S'FIRST+1..LEN));
			case S(S'FIRST) is
				when '+' => null;
				when '-' => VALUE := -VALUE;
				when others => raise UNKNOWN_OPERATOR;
			end case;
			if (VALUE = FREQUENCY) then
				ADDED := TRUE;
				exit;
			end if;
		end loop;
		if not(ADDED) then
			RESET(FILE_FREQ, APPEND_FILE);
			WRITE_FREQUENCY(FILE_FREQ,FREQUENCY);
		end if;
		CLOSE(FILE_FREQ);
		return not(ADDED);
	exception
		when ADA.IO_EXCEPTIONS.NAME_ERROR =>
			CREATE(FILE_FREQ, OUT_FILE, "visited_frequencies");
			WRITE_FREQUENCY(FILE_FREQ,FREQUENCY);
			CLOSE(FILE_FREQ);
			return TRUE;
	end ADD_FREQUENCY;
	procedure WRITE_FREQUENCY (FILE : FILE_TYPE; FREQUENCY : IN INTEGER) is
		S : STRING(1..10);
		LEN : INTEGER;
	begin
		LEN := STRING'(INTEGER'IMAGE(FREQUENCY))'LAST - STRING'(INTEGER'IMAGE(FREQUENCY))'FIRST + 1;
		S(1..LEN) := INTEGER'IMAGE(FREQUENCY);
		if (FREQUENCY >= 0) then
			S(2..LEN+1) := S(1..LEN);
			S(1) := '+';
			LEN := LEN + 1;
		end if;
		PUT_LINE(FILE, S(1..LEN));
	end WRITE_FREQUENCY;
end CHRONAL_CALIBRATION;
