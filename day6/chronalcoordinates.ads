with Ada.Containers.Vectors;

package ChronalCoordinates is
	type Coordinates_Type is
		record
			X : Natural;
			Y : Natural;
		end record;
	package CoordinatesVectors is new Ada.Containers.Vectors(Natural, Coordinates_Type);
	procedure ReadCoordinates(FileName : IN String ; Coordinates : OUT CoordinatesVectors.Vector);
	procedure PrintCoordinates(Coordinates : IN CoordinatesVectors.Vector);
	procedure FindLargestArea(FileName : IN String);
	procedure FindAreaLessTenThousand(FileName : IN String);
end ChronalCoordinates;
