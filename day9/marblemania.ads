with Ada.Containers.Vectors;

package MarbleMania is
	package NaturalVectors is new Ada.Containers.Vectors(Natural, Natural);
	type Score_Type is array (Natural range <>) of Natural;
	procedure PlayGame(PlayerNumber, LastMarbleNumber : IN Natural);
end MarbleMania;
